
module alu(output reg[12:0] alu_out,output zero_flag,input[2:0] alu_func,input[12:0] in1,in2);
	parameter ADD = 3'b000;
	parameter AND = 3'b001;
	parameter NOT = 3'b010;
	parameter ROT1R = 3'b011;
	parameter ROT1L = 3'b100;
	parameter ROT2R = 3'b101;
	parameter ROT2L = 3'b110;
	
	always@(alu_func,in1,in2) begin
		case(alu_func)
			ADD:
				alu_out = in1 + in2;
			AND:
				alu_out = in1 & in2;
			NOT:
				alu_out = ~in1;
			ROT1R:
				alu_out = {in1[0],in1[12:1]};
			ROT1L:
				alu_out = {in1[11:0],in1[12]};
			ROT2R:
				alu_out = {in1[1:0],in1[12:2]};			
			ROT2L:
				alu_out = {in1[10:0],in1[12:11]};
			default:
				alu_out = in1 + in2;
		endcase
	end
	assign zero_flage = ~(|{alu_out}); 
endmodule