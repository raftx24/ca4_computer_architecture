
module controller (
	output reg pc_sel=0,ir_write=0,IorD=0,is_jms=0,mdr_write=0,is_current_page=0,is_indirect=0,cy_write=0,acc_write=0,pc_write_enable=0,mem_read=0,mem_write=0,use_last_addr=0,write_last_addr=0,output reg[1:0] src_a=0,src_b =0,output reg[2:0] alu_func=0,
	input clk,rst,isMinusAcc,isZeroAcc,isNonZeroCy,zeroAlu,input[11:0] IR
);
	parameter ALU_ADD = 3'b000;
	parameter ALU_AND = 3'b001;
	parameter ALU_NOT = 3'b010;
	parameter ALU_ROTATE_ONE_RIGHT = 3'b011;
	parameter ALU_ROTATE_ONE_LEFT = 3'b100;
	parameter ALU_ROTATE_TWO_RIGHT = 3'b101;
	parameter ALU_ROTATE_TWO_LEFT = 3'b110;



	parameter OPCODE_GROUP_1 = 4'b1110;
	parameter OPCODE_GROUP_2 = 4'b1111;
	parameter OPCODE_GROUP_0_AND = 3'b000;
	parameter OPCODE_GROUP_0_ADD = 3'b001;
	parameter OPCODE_GROUP_0_ISZ = 3'b010;
	parameter OPCODE_GROUP_0_DLA = 3'b011;
	parameter OPCODE_GROUP_0_JMP = 3'b100;
	parameter OPCODE_GROUP_0_JMS = 3'b101;

	parameter STATE_IF = 0;
	parameter STATE_ID = 1;

	parameter STATE_G2 = 2;

	parameter STATE_G1_INC_ACC = 3;
	parameter STATE_G1_ROTATE = 4;
	parameter STATE_G1_COMP = 5;
	parameter STATE_G1_CLEAR = 6;

	parameter STATE_G0_INDIRECT = 7;
	parameter STATE_G0_AND = 8;
	parameter STATE_G0_ADD = 9;
	parameter STATE_G0_ISZ = 10;
	parameter STATE_G0_ISZ_2 = 14;
	parameter STATE_G0_DLA = 11;
	parameter STATE_G0_DLA_2 = 15;
	parameter STATE_G0_JMS = 12;
	parameter STATE_G0_JMP = 13;

	wire pb,ib;
	wire cla,clc,cma,cmc,rar,ral,rot,iac;
	wire sma,sza,snc;

	//group 0
	assign pb = IR[7];
	assign ib = IR[8];

	//group 1
	assign cla = IR[7];
	assign clc = IR[6];
	assign cma = IR[5];
	assign cmc = IR[4];
	assign rar = IR[3];
	assign ral = IR[2];
	assign rot = IR[1];
	assign iac = IR[0];

	//group 2
	assign sma = IR[7];
	assign sza = IR[6];
	assign snc = IR[5];

	reg[4:0] pstate,nstate;

	always @(pstate,isMinusAcc,isZeroAcc,isNonZeroCy,zeroAlu,IR) begin
	{pc_sel,ir_write,IorD,is_jms,mdr_write,cy_write,acc_write,pc_write_enable,mem_read,mem_write,use_last_addr,write_last_addr} <= 0;
		alu_func <= 3'b0;
		src_b <= 0;
		src_a <= 0;
		case(pstate)
			STATE_IF: begin
				{ir_write,IorD,pc_write_enable} <=3'b111;
				src_a <= 2;
				src_b <= 1 ;
				pc_sel <= 0;
				mem_read <= 1;
				alu_func <= ALU_ADD;
				nstate <= STATE_ID;
			end
			STATE_ID : begin
				is_indirect <= 0;
				is_current_page <= pb;
				IorD <= 0;
				mdr_write <=1;
				mem_read <= 1;
				if(IR[11:8] == OPCODE_GROUP_1)begin
					if(clc|cla)
						nstate<=STATE_G1_CLEAR;
					else if(cma | cmc)
						nstate<=STATE_G1_COMP;
					else if(rar|ral)
						nstate<=STATE_G1_ROTATE;
					else if(iac)
						nstate<=STATE_G1_INC_ACC;
				end else if(IR[11:8] == OPCODE_GROUP_2) begin
					nstate <= STATE_G2;
				end else begin
					if(ib)
						nstate <= STATE_G0_INDIRECT;
					else begin
						case(IR[11:9])
							OPCODE_GROUP_0_AND :begin
								nstate<=STATE_G0_AND;
							end
							OPCODE_GROUP_0_ADD :begin
								nstate<=STATE_G0_ADD;
							end
							OPCODE_GROUP_0_ISZ :begin
								nstate<=STATE_G0_ISZ;
							end
							OPCODE_GROUP_0_DLA :begin
								nstate<=STATE_G0_DLA;
							end
							OPCODE_GROUP_0_JMP :begin
								nstate<=STATE_G0_JMP;
							end
							OPCODE_GROUP_0_JMS :begin
								nstate<=STATE_G0_JMS;
							end
						endcase
					end
				end
			end
			STATE_G2:begin
				src_a <= 2;
				src_b<= 1;
				pc_sel <= 0;
				pc_write_enable<= ((sma & isMinusAcc)|(sza & isZeroAcc)|(snc & isNonZeroCy));
				nstate <=STATE_IF;
			end
			STATE_G1_INC_ACC:begin
				src_a <=0;
				src_b <= 1;
				alu_func <= ALU_ADD;
				acc_write <= 1;
				nstate <= STATE_IF;
			end
			STATE_G1_COMP:begin
				src_a <=0;
				alu_func <= ALU_NOT;
				acc_write <= (cma);
				cy_write <= (cmc);
				nstate <= STATE_IF;
			end
			STATE_G1_CLEAR:begin
				src_b <=2;
				alu_func <= ALU_AND;
				acc_write <= (cla);
				cy_write <= (clc);
				nstate <= STATE_IF;
			end
			STATE_G1_ROTATE:begin
				if(rar & !rot) 
					alu_func <= ALU_ROTATE_ONE_RIGHT;
				else if(rar & rot)
					alu_func <= ALU_ROTATE_TWO_RIGHT;
				else if(ral & !rot)
					alu_func <= ALU_ROTATE_ONE_LEFT;
				else if(ral & rot)
					alu_func <= ALU_ROTATE_TWO_LEFT;
				src_a <= 0;
				acc_write <= 1;
				cy_write <= 1;
				nstate <= STATE_IF;
			end
			STATE_G0_AND:begin
				src_a <= 0;
				src_b <= 0;
				alu_func <= ALU_AND;
				acc_write <= 1;
				nstate <= STATE_IF;
			end
			STATE_G0_ADD:begin
				src_a <= 0;
				src_b <= 0;
				alu_func <= ALU_ADD;
				acc_write <= 1;
				cy_write <= 1;
				nstate <= STATE_IF;
			end
			STATE_G0_ISZ:begin
				src_a <= 1;
				src_b <= 0;
				alu_func <= ALU_ADD;
				mem_write <= 1;
				is_jms <= 0;
				IorD <= 0;
				is_indirect <= ib;
				use_last_addr <=ib;
				nstate <= STATE_G0_ISZ_2;
			end
			STATE_G0_ISZ_2:begin
				src_a <= 2;
				src_b <= 1;
				alu_func <= ALU_ADD;
				pc_write_enable <= zeroAlu;
				pc_sel <= 0;
				nstate <= STATE_IF;
			end
			STATE_G0_DLA:begin
				src_a <= 0;
				src_b <= 2;
				alu_func <= ALU_ADD;
				mem_write <= 1;
				IorD <= 0;
				is_jms <= 0;
				nstate <= STATE_G0_DLA_2;
			end
			STATE_G0_DLA_2 : begin
				src_a <= 0;
				src_b <= 2;
				alu_func <= ALU_AND;
				acc_write<= 1;
				nstate <= STATE_IF;
			end
			STATE_G0_JMS:begin
				is_jms<=1;
				IorD <= 0;
				mem_write <= 1;
				src_a <= 1;
				src_b <= 0;
				alu_func <=ALU_ADD;
				pc_sel <= 0;
				pc_write_enable <= 1;
				nstate <= STATE_IF;
			end
			STATE_G0_JMP:begin
				pc_sel <= 0;
				pc_write_enable <= 1;
				nstate <= STATE_IF;
			end
			STATE_G0_INDIRECT:begin
				write_last_addr <= 1;
				is_indirect <= 1;
				IorD <= 0;
				mem_read <= 1;
				mdr_write <=1;
				case(IR[11:9])
					OPCODE_GROUP_0_AND :begin
						nstate<=STATE_G0_AND;
					end
					OPCODE_GROUP_0_ADD :begin
						nstate<=STATE_G0_ADD;
					end
					OPCODE_GROUP_0_ISZ :begin
						nstate<=STATE_G0_ISZ;
					end
					OPCODE_GROUP_0_DLA :begin
						nstate<=STATE_G0_DLA;
					end
					OPCODE_GROUP_0_JMP :begin
						nstate<=STATE_G0_JMP;
					end
					OPCODE_GROUP_0_JMS :begin
						nstate<=STATE_G0_JMS;
					end
				endcase
			end


		endcase
	end
	always @(posedge clk) begin
		if(rst) pstate <= STATE_IF;
			else pstate <= nstate;
	end
endmodule
