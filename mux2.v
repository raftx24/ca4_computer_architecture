
module mux2 #(parameter WIDTH = 32) 
	(output [WIDTH-1:0] out,input[WIDTH-1:0] in0,in1,input sel,input enable);
	assign out = (enable == 1) ? (sel == 0) ? in0 :
				 (sel == 1) ? in1 : 'z : 'z;
	
endmodule

