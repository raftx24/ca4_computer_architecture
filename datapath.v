
module datapath(output zero_acc,non_zero_cy,minus_acc,zero_flag,
					output[11:0] IR,
					input ir_write,pc_sel,pc_write,mdr_write,mem_read,mem_write,use_last_addr,write_last_addr,cy_write,acc_write,is_jms,iord,ib,pb,
					input[1:0] src_a,src_b,
					input[2:0] alu_func,
					input clk,rst);

	wire[11:0] pc_out,pc_in;
	wire[11:0] mem_read_data,mem_address,mem_write_data;
	wire[11:0] ir_out;
	wire[11:0] mdr_out;
	wire[11:0] pb_mux_out,ib_mux_out,last_addr;
	wire[11:0] ea_addr;
	wire[11:0] acc_out,acc_in;
	wire[12:0] alu_out;
	wire cy_out,cy_in;
	wire[12:0] alu_in1,alu_in2;
	wire temp_zero;

	
	assign minus_acc = acc_out[11];
	assign non_zero_cy = cy_out;
	assign zero_acc = ~(|{acc_out});
	assign IR = ir_out;
	// PC
	register #(12) PC(pc_out,pc_in,pc_write,rst,clk);
	mux2 #(12) PC_MUX(pc_in,alu_out[11:0],ib_mux_out,pc_sel,1'b1);
	// Memory Section
	memory #(12,2**12,12,12) MEMORY(mem_read_data,mem_address,mem_write_data,mem_read,mem_write,rst,clk);
	mux2 #(12) ADDR_MUX(mem_address,ea_addr,pc_out,iord,1'b1);
	mux2 #(12) WRITE_MUX(mem_write_data,alu_out[11:0],pc_out,is_jms,1'b1);
	// IR
	register #(12) IR_REG(ir_out,mem_read_data,ir_write,rst,clk);
	// MDR
	register #(12) MDR(mdr_out,mem_read_data,mdr_write,rst,clk);
	// Effective Address Calculation
	register #(12) LAST_ADDR_REG(last_addr,mdr_out,write_last_addr,rst,clk);
	mux2 #(12) PB_MUX(pb_mux_out,{5'b0,ir_out[6:0]},{pc_out[11:7],ir_out[6:0]},pb,1'b1);
	mux2 #(12) IB_MUX(ib_mux_out,pb_mux_out,mdr_out,ib | is_jms,1'b1);
	mux2 #(12) USE_LAST_ADDRESS_MUX(ea_addr,ib_mux_out,last_addr,use_last_addr,1'b1);
	// ACC & CY
	register #(12) ACC(acc_out,alu_out[11:0],acc_write,rst,clk);
	register #(1) CY(cy_out,alu_out[12],cy_write,rst,clk);
	// ALU
	alu ALU(alu_out,temp_zero,alu_func,alu_in1,alu_in2);
	mux4 #(13) SRC_A_MUX(alu_in1,{cy_out,acc_out},13'b0000000000001,{1'b0,pc_out},13'b0,src_a,1'b1);
	mux4 #(13) SRC_B_MUX(alu_in2,{1'b0,mdr_out},{13'b0000000000001},13'b0,13'b0,src_b,1'b1);
	register #(1) ZERO_FL(zero_flag,temp_zero,1'b1,rst,clk);
	//
	
	
endmodule
