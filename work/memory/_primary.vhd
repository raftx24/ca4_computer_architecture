library verilog;
use verilog.vl_types.all;
entity memory is
    generic(
        WIDTH           : integer := 12;
        SIZE            : integer := 4096;
        LOGSIZE         : integer := 12;
        VIRTUAL_LOGSIZE : integer := 12
    );
    port(
        read_data       : out    vl_logic_vector;
        address         : in     vl_logic_vector;
        write_data      : in     vl_logic_vector;
        mem_read        : in     vl_logic;
        mem_write       : in     vl_logic;
        \_rst\          : in     vl_logic;
        clk             : in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of WIDTH : constant is 1;
    attribute mti_svvh_generic_type of SIZE : constant is 1;
    attribute mti_svvh_generic_type of LOGSIZE : constant is 1;
    attribute mti_svvh_generic_type of VIRTUAL_LOGSIZE : constant is 1;
end memory;
