library verilog;
use verilog.vl_types.all;
entity alu is
    generic(
        ADD             : vl_logic_vector(0 to 2) := (Hi0, Hi0, Hi0);
        \AND\           : vl_logic_vector(0 to 2) := (Hi0, Hi0, Hi1);
        \NOT\           : vl_logic_vector(0 to 2) := (Hi0, Hi1, Hi0);
        ROT1R           : vl_logic_vector(0 to 2) := (Hi0, Hi1, Hi1);
        ROT1L           : vl_logic_vector(0 to 2) := (Hi1, Hi0, Hi0);
        ROT2R           : vl_logic_vector(0 to 2) := (Hi1, Hi0, Hi1);
        ROT2L           : vl_logic_vector(0 to 2) := (Hi1, Hi1, Hi0)
    );
    port(
        alu_out         : out    vl_logic_vector(12 downto 0);
        zero_flag       : out    vl_logic;
        alu_func        : in     vl_logic_vector(2 downto 0);
        in1             : in     vl_logic_vector(12 downto 0);
        in2             : in     vl_logic_vector(12 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of ADD : constant is 1;
    attribute mti_svvh_generic_type of \AND\ : constant is 1;
    attribute mti_svvh_generic_type of \NOT\ : constant is 1;
    attribute mti_svvh_generic_type of ROT1R : constant is 1;
    attribute mti_svvh_generic_type of ROT1L : constant is 1;
    attribute mti_svvh_generic_type of ROT2R : constant is 1;
    attribute mti_svvh_generic_type of ROT2L : constant is 1;
end alu;
