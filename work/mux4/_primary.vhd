library verilog;
use verilog.vl_types.all;
entity mux4 is
    generic(
        WIDTH           : integer := 32
    );
    port(
        \out\           : out    vl_logic_vector;
        in0             : in     vl_logic_vector;
        in1             : in     vl_logic_vector;
        in2             : in     vl_logic_vector;
        in3             : in     vl_logic_vector;
        sel             : in     vl_logic_vector(1 downto 0);
        enable          : in     vl_logic
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of WIDTH : constant is 1;
end mux4;
