library verilog;
use verilog.vl_types.all;
entity datapath is
    port(
        zero_acc        : out    vl_logic;
        non_zero_cy     : out    vl_logic;
        minus_acc       : out    vl_logic;
        zero_flag       : out    vl_logic;
        IR              : out    vl_logic_vector(11 downto 0);
        ir_write        : in     vl_logic;
        pc_sel          : in     vl_logic;
        pc_write        : in     vl_logic;
        mdr_write       : in     vl_logic;
        mem_read        : in     vl_logic;
        mem_write       : in     vl_logic;
        use_last_addr   : in     vl_logic;
        write_last_addr : in     vl_logic;
        cy_write        : in     vl_logic;
        acc_write       : in     vl_logic;
        is_jms          : in     vl_logic;
        iord            : in     vl_logic;
        ib              : in     vl_logic;
        pb              : in     vl_logic;
        src_a           : in     vl_logic_vector(1 downto 0);
        src_b           : in     vl_logic_vector(1 downto 0);
        alu_func        : in     vl_logic_vector(2 downto 0);
        clk             : in     vl_logic;
        rst             : in     vl_logic
    );
end datapath;
