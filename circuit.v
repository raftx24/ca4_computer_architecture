
module circuit(input clk,rst);
	wire pc_sel,ir_write,iord,is_jms,mdr_write,pb,ib,cy_write,acc_write,pc_write,mem_read,mem_write,use_last_addr,write_last_addr;
	wire[1:0] src_a,src_b;
	wire[2:0] alu_func;
	wire isMinusAcc,isZeroAcc,isNonZeroCy,zero_flag;
	wire[11:0] IR;
	controller CONTROLLER(pc_sel,ir_write,iord,is_jms,mdr_write,pb,ib,cy_write,acc_write,
					pc_write,mem_read,mem_write,use_last_addr,write_last_addr,src_a,src_b,alu_func,clk,rst,isMinusAcc,isZeroAcc,isNonZeroCy,zero_flag,IR);
	datapath DATAPATH(isZeroAcc,isNonZeroCy,isMinusAcc,zero_flag,
					IR,ir_write,pc_sel,pc_write,mdr_write,mem_read,mem_write,use_last_addr,write_last_addr,cy_write,acc_write,is_jms,iord,ib,pb,
					src_a,src_b,
					alu_func,
					clk,rst);
endmodule
